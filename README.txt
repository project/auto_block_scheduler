== Description ==
Auto Block Scheduler allows the administrator to schedule enable/disable Blocks
based on a pre-defined schedule.

The Administrator have to feed publish and un-publish dates
and the Auto Block Schedule will take care of enabling/disabling the block as
per the given dates.


== Notes ==
1. Auto block Scheduler only does publishing and un-publishing of blocks.

Installation
------------
1. Copy the entire  directory of auto_block_scheduler to Drupal
/modules or /modules/contrib directory.

2. Login as an administrator to Enable the module in the "Administer" ->
   Modules (admin/modules)

3. Access the links to schedule enable/disable Blocks.
   (admin/structure/block/auto-block-scheduler)

== Author ==
Arulraj
